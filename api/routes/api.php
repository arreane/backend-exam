<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'UsersController@authenticate');
Route::post('/register', 'UsersController@register');
Route::get('posts', 'PostsController@index');
Route::get('posts/{slug}', 'PostsController@show');
Route::group(['middleware' => ['auth.api']], function(){
	Route::post('posts', 'PostsController@store');
	Route::patch('posts/{slug}', 'PostsController@update');
	Route::delete('posts/{slug}', 'PostsController@destroy');
});
