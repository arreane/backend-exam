<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UsersController extends Controller
{
    public function authenticate(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'email' => 'required',
    		'password' => 'required'
    	]);
    	if($validator->fails()){
    		$message = 'The given data was invalid.';
    		$errors = $validator->errors();
            return response()->json(compact('message', 'errors'), 422);
    	}

        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
            	$message = 'The given data was invalid.';
        		$errors = ['email' => ['These credentials do not match our records.']];
                return response()->json(compact('message', 'errors'), 422);
            }
        } catch (JWTException $e) {
        	$message = 'could not create token';
    		$errors = ['errors' => 'internal error'];
            return response()->json(compact('message', 'errors'), 500);
        }

        $response = [
        	'token' => $token,
        	'token_type' => 'bearer',
        	'expires_at' => date('Y-m-d h:i:s')
        ];

        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed',
        ]);

        if($validator->fails()){
        	$message = 'The given data was invalid.';
    		$errors = $validator->errors();
            return response()->json(compact('message', 'errors'), 422);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        // $token = JWTAuth::fromUser($user);

        return response()->json($user,201);
    }

    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        	return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
        	return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
        	return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }
}