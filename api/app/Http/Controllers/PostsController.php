<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Post::latest()->take(20)->get();
        return response()->json(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'image' => 'url'
        ]);
        
        if($validator->fails()){
            $message = 'The given data was invalid.';
            $errors = $validator->errors();
            return response()->json(compact('message', 'errors'), 422);
        }

        $data = Post::create([
            'title' => request('title'),
            'slug' => str_replace(' ', '-', request('title')),
            'content' => request('content'),
            'user_id' => user()->id,
            'image' => request('image')
        ]);

        return response()->json(compac('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();
        return response()->json(['data' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();

        $post->update($request->only('title', 'content', 'image'));

        return response()->json(['data' => $post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
       $post = Post::where('slug', $slug)->firstOrFail();
        $post->delete();
        return response()->json(["status" => "record deleted successfully"]);
    }
}
